const cheerio = require('cheerio')
const rp = require('request-promise')

async function Main() {
   const $ = await rp({
       uri: 'http://quotes.toscrape.com/',
       transform: body => cheerio.load(body)
   })
//    const containerClass = $('.row .col-md-8').parent().next();
//    console.log(containerClass.html());
$('.quotes').each((i,el)=>{
    const text = $(el).find('span.text').text().replace(/(^\“|\”$)/g,"");
    const author = $(el).find('span small.author').text();
    const tags = [];
    $(el).find('.tags a.tag').each((i, el)=> tags.push($(el).text()))
        console.log(tags.join(','))
    
})
   
   
}

Main();